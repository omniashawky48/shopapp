import 'dart:convert';
import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop_app/model/http_exception.dart';


class Auth with ChangeNotifier {
  String _token;
  String _userId;
  Timer _authTimer;
  DateTime _expiryDate;
  bool _type;
  bool get isAuth {
    return token != null;
  }

  bool get type{
    return _type;
  }

  String get token {
    if (_expiryDate != null &&
        _expiryDate.isAfter(DateTime.now()) &&
        _token != null&&_type!=null) {
      return _token;
    }
    return null;
  }

  String get userId {
    return _userId;
  }

  Future<void> _authenticate(
      String email, String password, String urlSegment) async {
    final url =
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/$urlSegment?key=AIzaSyDYGfBtNk-cm02e10t6mqAKfY-luUBVEnc';

    try {
      final response = await http.post(
        url,
        body: json.encode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) { //stats code ==200 but there is error in body
        throw HttpException(responseData['error']['message']);
      }
      _token = responseData['idToken'];
      _userId = responseData['localId'];
      _expiryDate = DateTime.now().add(Duration(seconds: int.parse(responseData['expiresIn'],),),);

      final userTypeUrl= 'https://fluttertest-8b52d.firebaseio.com/users.json?auth=$_token';
      try{
        final responce = await http.get(userTypeUrl);
        final extracteddate = json.decode(responce.body) as Map<String, dynamic>;
        print(' ussssseeeeeeeeeertype ${extracteddate.toString()}');
        extracteddate.forEach((user, userData) {
          if(_userId.contains(userData['user']))
       {
         _type=userData['type'];
       }
        });
      }
      catch(e){throw e;}
      _autoLogout();
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      var isVendor=prefs.getBool('vendorSignup');
      if(urlSegment.contains('signupNewUser'))
        {
          try {
            await http.post(userTypeUrl,
                body: json.encode({
                  'user': userId,
                  'type': isVendor,
                }));
          }catch(e){throw e;}
        }

      final userData = json.encode(
        {
          'token': _token,
          'userId': _userId,
          'expiryDate': _expiryDate.toIso8601String(),
          'type':_type
        },
      );
      prefs.setString('userData', userData);
    } catch (error) {
      throw error;
    }
    notifyListeners();
  }

  Future<void> signup(String email, String password) async {
    return _authenticate(email, password, 'signupNewUser'); //return for excutes await
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'verifyPassword');
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData = json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _expiryDate = expiryDate;
    _type=extractedUserData['type'];
    notifyListeners();
    _autoLogout();
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
//     prefs.remove('userData');
    prefs.clear();
  }

  void _autoLogout() {
    if (_authTimer != null||_type==null) {
      _authTimer.cancel();
    }
    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
