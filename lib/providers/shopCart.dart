
import 'package:flutter/cupertino.dart';
class CartItem{
  final String id;
  final String title;
  final int quantity;
  final double price;

  CartItem({@required this.id,@required  this.title,@required  this.quantity,@required  this.price});
}

class CartProvider with ChangeNotifier{
  Map<String,CartItem> cart={};

  Map<String,CartItem> get carts
  {
    return {...cart};
  }

  void addItem(String id,String title,double price){

    if(cart.containsKey(id))
      {
        cart.update(id, (cart)=>CartItem(id: cart.id, title: cart.title, quantity: cart.quantity+1, price: cart.price));
      }
    else{
      cart.putIfAbsent(id,()=>CartItem(title: title,id: DateTime.now().toString(),price: price,quantity: 1));
    }
    notifyListeners();
  }


  void removeSingleItem(String poductid){

    if(!cart.containsKey(poductid))
    {
      return;    }
    if(cart[poductid].quantity>1){
      cart.update(poductid,(exis)=>CartItem(title: exis.title,id: exis.id,price: exis.price,quantity: exis.quantity-1));
    }else{
      cart.remove(poductid);
    }
    notifyListeners();
  }



  void removeItem(String productId) {
    cart.remove(productId);
    notifyListeners();
  }

  int get itemCount{
    return cart.length;
  }

  double get itemTotal{
    double total=0;
     cart.forEach((key,val){
       total+= (val.price)*(val.quantity);
    });
    return total;
  }


  void clear()
  {
    cart={};
    notifyListeners();
  }

}