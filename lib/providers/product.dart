import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class Product with ChangeNotifier {
  final String title;
  final String id;
  final String description;
  final String imageUrl;
  final double price;
  bool isfavourite;

  Product(
      {@required this.title,
      @required this.id,
      @required this.description,
      @required this.imageUrl,
      @required this.price,
      this.isfavourite = false});

  Future<void> toggleFavourite(String authToken,String userId) async {
    var oldValue = isfavourite;
    isfavourite = !isfavourite;
    notifyListeners();
    var url = 'https://fluttertest-8b52d.firebaseio.com/userFavorites/$userId/$id.json?auth=$authToken';
   try{
     final response=await http.patch(url, body: json.encode({'isfavourite': isfavourite}));

     if(response.statusCode>=400)
       {
         isfavourite=oldValue;
         notifyListeners();
       }

   }catch(error){
     isfavourite=oldValue;
     notifyListeners();
   }

  }
}
