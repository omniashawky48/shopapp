import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/localization_delegate.dart';
import 'package:flutter_translate/localization_provider.dart';
import 'package:flutter_translate/localized_app.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/auth_provider.dart';
import 'package:shop_app/providers/orders.dart';
import 'package:shop_app/providers/product.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'package:shop_app/providers/shopCart.dart';
import 'package:shop_app/screens/auth-screen.dart';
import 'package:shop_app/screens/edit_product_screen.dart';
import 'package:shop_app/screens/orders_screen.dart';
import 'package:shop_app/screens/product_detail_screen.dart';
import 'package:shop_app/screens/products_overview_screen.dart';
import 'package:shop_app/screens/shopping_cart_screen.dart';
import 'package:shop_app/screens/user_products_screen.dart';



void main()async {
  var delegate = await LocalizationDelegate.create(
      fallbackLocale: 'en_US', supportedLocales: ['en_US', 'ar']);
  runApp(LocalizedApp(delegate, MyApp()));

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;
    return LocalizationProvider(
      state: LocalizationProvider.of(context).state,
      child: MultiProvider(
          providers: [
            ChangeNotifierProvider.value(
              value: Auth(),
            ),
            ChangeNotifierProxyProvider<Auth, Products>(
              builder: (ctx, auth, previousProducts) => Products(
                auth.token,
                auth.userId,
                previousProducts == null ? [] : previousProducts.items,
              ),
            ),
            ChangeNotifierProvider.value(
                value: Product()),
            ChangeNotifierProvider.value(
                value: CartProvider()),
            ChangeNotifierProxyProvider<Auth, Orders>(
              builder: (ctx, auth, previousOrders) => Orders(
                auth.token,
                auth.userId,
                previousOrders == null ? [] : previousOrders.orders,
              ),
            ),
          ],
          child: Consumer<Auth>(builder: (con,auth,_)=>MaterialApp(
            title: 'Flutter Demo',
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate, localizationDelegate
            ],
            supportedLocales: localizationDelegate.supportedLocales,
            locale: localizationDelegate.currentLocale,
            theme: ThemeData(
                primaryColor: Colors.orange,
                accentColor: Colors.purple
            ),
            debugShowCheckedModeBanner: false,
            home:  auth.isAuth
                ? ProductsOverviewScreen(auth.type)
                : FutureBuilder(
              future: auth.tryAutoLogin(),
              builder: (ctx, authResultSnapshot) =>
              authResultSnapshot.connectionState ==
                  ConnectionState.waiting
                  ? AuthScreen()
                  : AuthScreen(),
            ),
            routes:{

              ProductsOverviewScreen.productsOverviewScreenRoute:(context)=>(auth.isAuth?ProductsOverviewScreen(auth.type):AuthScreen()),
              ProductDetailsScreen.productDetailsScreenRoute:(context)=>(auth.isAuth?ProductDetailsScreen():AuthScreen()),
              ShoppingCartScreen.shoppingCartScreenRoute:(context)=>(auth.isAuth?ShoppingCartScreen(auth.type):AuthScreen()),
              OrdersScreen.routeName:(context)=>auth.isAuth?OrdersScreen(auth.type):AuthScreen(),
              UserProductsScreen.userRoute:(context)=>auth.isAuth?UserProductsScreen(auth.type):AuthScreen(),
              EditProductScreen.editProductRoute:(context)=>auth.type?EditProductScreen():AuthScreen(),
//              SplashScreen.routeName:(context)=>SplashScreen(),
          AuthScreen.routeName:(context)=>AuthScreen(),

            },
          )),
      ),
    );
  }


}
