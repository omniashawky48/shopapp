
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/global.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'package:shop_app/providers/shopCart.dart';
import 'package:shop_app/screens/shopping_cart_screen.dart';
import 'package:shop_app/widgwts/app_drawer.dart';
import 'package:shop_app/widgwts/badge.dart';
import 'package:shop_app/widgwts/product_item.dart';

enum Favourite { OnlyFav, ShowAll }

class ProductsOverviewScreen extends StatefulWidget {
  static const productsOverviewScreenRoute = '/product-overview-screen';
  final isVendor;
  ProductsOverviewScreen(this.isVendor);

  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var _loadeditemsfav = false; //////////////////////////

  Future<void> _refreshProducts(BuildContext context) async {
//    final prefs = await SharedPreferences.getInstance();
//     var extractedUserData = json.decode(prefs.getString('userData')) as Map<String, Object>;
//     isVendor=extractedUserData['type'];
    print('ttyypp ${widget.isVendor}');
    await Provider.of<Products>(context, listen: false)
        .fetchAndSetProducts(widget.isVendor);
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(translate('rusure_to_exit')),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                    child: Text(translate('button.ok'))),
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: Text(translate('button.cancel'))),
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    final loadedProductsdata = Provider.of<Products>(context, listen: false);
    final loadedProducts = _loadeditemsfav
        ? loadedProductsdata.productsFavouriteItems
        : loadedProductsdata.items;

    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Scaffold(
          appBar: AppBar(
            title: Text(translate('app_bar.title')),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.language),
                  onPressed: () {
                    _onActionSheetPress(context);
                  }),
              Consumer<CartProvider>(
                builder: (_, cart, _c) => Badge(
                    child: IconButton(
                        icon: Icon(Icons.shopping_cart),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              ShoppingCartScreen.shoppingCartScreenRoute);
                        }),
                    value: cart.itemCount.toString()),
              ),
              if (!widget.isVendor)
                PopupMenuButton(
                  onSelected: (Favourite favFilter) {
                    setState(() {
                      if (favFilter == Favourite.OnlyFav) {
                        _loadeditemsfav = true;
                      } else {
                        _loadeditemsfav = false;
                      }
                    });
                  },
                  icon: Icon(Icons.more_vert),
                  itemBuilder: (con) => [
                    PopupMenuItem(
                      child: Text(translate('app_bar.onlyfav')),
                      value: Favourite.OnlyFav,
                    ),
                    PopupMenuItem(
                      child: Text(translate('app_bar.showall')),
                      value: Favourite.ShowAll,
                    ),
                  ],
                ),
            ],
          ),
          drawer: AppDrawer(
            isvendor: widget.isVendor,
          ),
          body: FutureBuilder(
            builder: (context, dataSnapshot) {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                if (dataSnapshot.error != null) {
                  return Center(
                    child: Text(dataSnapshot.error.toString()),
                  );
                } else {
                  return RefreshIndicator(
                    onRefresh: () => _refreshProducts(context),
                    child: GridView.builder(
                      padding: const EdgeInsets.all(10),
                      itemCount: loadedProducts.length,
                      itemBuilder: (context, index) =>
                          ChangeNotifierProvider.value(
                              value: loadedProducts[index],
                              child: ProductItem(widget.isVendor)),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 10,
                          crossAxisSpacing: 10,
                          childAspectRatio: 3 / 5),
                    ),
                  );
                }
              }
            },
            future: _refreshProducts(context),
          )),
    );
  }

  void showDemoActionSheet({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
        context: context,
        builder: (BuildContext context) => child).then((String value) {
      changeLocale(context, value);
    });
  }

  void _onActionSheetPress(BuildContext context) {
    showDemoActionSheet(
      context: context,
      child: CupertinoActionSheet(
        title: Text(translate('language.selection.title')),
        message: Text(translate('language.selection.message')),
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: Text(translate('language.name.en')),
            onPressed: () => Navigator.pop(context, 'en_US'),
          ),
          CupertinoActionSheetAction(
            child: Text(translate('language.name.ar')),
            onPressed: () => Navigator.pop(context, 'ar'),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text(translate('button.cancel')),
          isDefaultAction: true,
          onPressed: () => Navigator.pop(context, null),
        ),
      ),
    );
  }
}
