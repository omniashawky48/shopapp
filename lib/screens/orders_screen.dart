import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/widgwts/app_drawer.dart';
import 'package:shop_app/widgwts/order_item.dart';

import '../providers/orders.dart' show OrderItem, Orders;

class OrdersScreen extends StatelessWidget {
  static const routeName = '/orders';
  final authType;
  OrdersScreen(this.authType);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate('your_order')),
      ),
      drawer: AppDrawer(isvendor: authType,),
      body: FutureBuilder(builder:(con,dataSnapshot){
        if (dataSnapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else {
          if (dataSnapshot.error != null) {
            // ...
            // Do error handling stuff
            return Center(
              child: Text(dataSnapshot.error.toString()),
            );
          } else {
            return Consumer<Orders>(
              builder: (ctx, orderData, child) => ListView.builder(
                itemCount: orderData.orders.length,
                itemBuilder: (ctx, i) => OrderItemWidget(orderData.orders[i]),
              ),
            );
          }
        }
        } ,
      future: Provider.of<Orders>(context,listen: false).fetchAndSetOrders() ,)
    );
  }
}
