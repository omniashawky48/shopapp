import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'package:shop_app/widgwts/app_drawer.dart';
import 'package:shop_app/widgwts/user_product_item.dart';

import 'edit_product_screen.dart';

class UserProductsScreen extends StatelessWidget {
  static const userRoute = '/user-route';
  final authType;
  UserProductsScreen(this.authType);
  @override
  Widget build(BuildContext context) {
    Future<void> _refreshProducts(BuildContext context) async {
      await Provider.of<Products>(context, listen: false)
          .fetchAndSetProducts(authType);
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Timer(Duration(seconds: 1), () {
                Navigator.of(context)
                    .pushNamed(EditProductScreen.editProductRoute);
              });
            }),
      ),
      appBar: AppBar(
        title: Text(translate('your_products')),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(EditProductScreen.editProductRoute);
              })
        ],
      ),
      drawer: AppDrawer(
        isvendor: authType,
      ),
      body: FutureBuilder(
          future: _refreshProducts(context),
          builder: (con, dataSnapshot) {
            if (dataSnapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else {
              if (dataSnapshot.error != null) {
                return Center(
                  child: Text(dataSnapshot.error.toString()),
                );
              } else {
                return RefreshIndicator(
                  onRefresh: () => _refreshProducts(context),
                  child: Consumer<Products>(
                    builder: (con, product, _) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListView.builder(
                        itemBuilder: (context, index) => Column(
                          children: <Widget>[
                            UserProductItem(
                                product.items[index].id,
                                product.items[index].title,
                                product.items[index].imageUrl),
                            Divider(),
                          ],
                        ),
                        itemCount: product.items.length,
                      ),
                    ),
                  ),
                );
              }
            }
          }),
    );
  }
}
