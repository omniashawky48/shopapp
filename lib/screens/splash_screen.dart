//
//import 'dart:async';
//
//import 'package:flutter/material.dart';
//import 'package:shop_app/screens/products_overview_screen.dart';
//
//class SplashScreen extends StatefulWidget {
//  static const routeName='/splash';
//
//  @override
//  _SplashScreenState createState() => _SplashScreenState();
//}
//
//class _SplashScreenState extends State<SplashScreen> {
//
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    Timer(Duration(seconds: 3),
//            () => Navigator.pushReplacementNamed(context, ProductsOverviewScreen.productsOverviewScreenRoute) );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      backgroundColor: Colors.deepOrange,
//      body: Stack(
//        fit: StackFit.expand,
//        children: <Widget>[
////          Container(
//////            decoration: BoxDecoration(color: Colors.redAccent),
////          ),
//          Column(
//            mainAxisAlignment: MainAxisAlignment.start,
//            children: <Widget>[
//              Expanded(
//                flex: 2,
//                child: Container(
//                  child: Column(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      CircleAvatar(
//                        backgroundColor: Colors.white,
//                        radius: 50.0,
//                        child: Icon(
//                          Icons.shopping_cart,
//                          color: Colors.greenAccent,
//                          size: 50.0,
//                        ),
//                      ),
//                      Padding(
//                        padding: EdgeInsets.only(top: 10.0),
//                      ),
//                      Text(
//                        'Shop App',
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontWeight: FontWeight.bold,
//                            fontSize: 24.0),
//                      )
//                    ],
//                  ),
//                ),
//              ),
//              Expanded(
//                flex: 1,
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    CircularProgressIndicator(backgroundColor: Colors.green,),
//                    Padding(
//                      padding: EdgeInsets.only(top: 20.0),
//                    ),
//                    Text(
//                      'Store',
//                      softWrap: true,
//                      textAlign: TextAlign.center,
//                      style: TextStyle(
//                          fontWeight: FontWeight.bold,
//                          fontSize: 18.0,
//                          color: Colors.white),
//                    )
//                  ],
//                ),
//              )
//            ],
//          )
//        ],
//      ),
//    );
//  }
//}
