import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'package:shop_app/providers/shopCart.dart';
import 'package:shop_app/screens/shopping_cart_screen.dart';
import 'package:shop_app/widgwts/badge.dart';

class ProductDetailsScreen extends StatelessWidget {
  static const productDetailsScreenRoute = '/product-Details-Screen-Route';
  @override
  Widget build(BuildContext context) {
    var products = Provider.of<Products>(context);
    String id = ModalRoute.of(context).settings.arguments as String;
    var Cart = Provider.of<CartProvider>(context, listen: false);
    var product = products.findProductById(id);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Color(0xFF545D68)),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text('Categoy',
            style: TextStyle(fontSize: 20.0, color: Color(0xFF545D68))),
        actions: <Widget>[
          Consumer<CartProvider>(
            builder: (_,cart,_c)=> Badge(
                child: IconButton(
                    icon: Icon(Icons.shopping_cart, color: Color(0xFF545D68)),
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed(ShoppingCartScreen.shoppingCartScreenRoute);
                    }),
                value: cart.itemCount.toString()),

          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Text(product.title,
                    style: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFFF17532))),
              ),
            ),
            Container(
                height: 200,
                width: double.infinity,
                child: Hero(
                    tag: id,
                    transitionOnUserGestures: true,
                    child: Image.network(
                      product.imageUrl,
                      fit: BoxFit.cover,
                    ))),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'price : \$${product.price}',
                textAlign: TextAlign.center,
                style: TextStyle(  color: Color(0xFF575E67), fontSize: 18),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                product.description,
                textAlign: TextAlign.center,
                style: TextStyle(color: Color(0xFFB4B8B9), fontSize: 18),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
                child: InkWell(
              onTap: () {
                Cart.addItem(product.id, product.title, product.price);
                Scaffold.of(context).hideCurrentSnackBar();
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(translate('add_item_to_cart')),
                  duration: Duration(seconds: 3),
                  backgroundColor: Colors.amber,
                  action: SnackBarAction(
                      label: translate('undo'),
                      onPressed: () {
                        Cart.removeSingleItem(product.id);
                      }),
                ));
              },
              child: Container(
                  width: MediaQuery.of(context).size.width - 50.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25.0),
                      color: Color(0xFFF17532)),
                  child: Center(
                      child: Text(
                    'Add to cart',
                    style: TextStyle(
                        fontFamily: 'Varela',
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ))),
            ))
          ],
        ),
      ),
    );
  }
}
