import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/orders.dart';
import 'package:shop_app/providers/shopCart.dart';
import 'package:shop_app/widgwts/app_drawer.dart';
import 'package:shop_app/widgwts/cart_item.dart';

class ShoppingCartScreen extends StatelessWidget {
  static const shoppingCartScreenRoute='/Shopping-Cart-Screen';
  final authType;
  ShoppingCartScreen(this.authType);

  @override
  Widget build(BuildContext context) {
   var  cart=Provider.of<CartProvider>(context);
    return Scaffold(
      appBar: AppBar(title: Text(translate('orders')),),
      drawer: AppDrawer(isvendor: authType,),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(15),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    translate('total'),
                    style: TextStyle(fontSize: 20),
                  ),

                  Chip(
                    label: Text(
                      '\$${cart.itemTotal.toStringAsFixed(2)}',
                      style: TextStyle(
                        color: Theme.of(context).primaryTextTheme.title.color,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  Spacer(),
                  OrderButtonn(cart),
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemCount: cart.cart.length,
              itemBuilder: (ctx, i) =>
                  CartItemWidget(
                cart.cart.values.toList()[i].id,
                cart.cart.keys.toList()[i],
                cart.cart.values.toList()[i].price,
                cart.cart.values.toList()[i].quantity,
                cart.cart.values.toList()[i].title,
              ),
            ),
          )
        ],
      ),
    );
  }
}


class OrderButtonn extends StatefulWidget {
  final CartProvider cart;

  OrderButtonn(this.cart);

  @override
  _OrderButtonnState createState() => _OrderButtonnState();
}

class _OrderButtonnState extends State<OrderButtonn> {
  var _isLoading = false;
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: _isLoading?CircularProgressIndicator(): Text(translate('order_now')),
      onPressed:(widget.cart.itemTotal==0||_isLoading) ?null: () async {
        setState(() {
          _isLoading=true;
        });
        await  Provider.of<Orders>(context, listen: false).addOrder(
          widget.cart.cart.values.toList(),
         widget. cart.itemTotal,
        );
        setState(() {
          _isLoading=false;
        });
        widget.cart.clear();
      },
      textColor: Theme.of(context).primaryColor,
    );
  }
}
