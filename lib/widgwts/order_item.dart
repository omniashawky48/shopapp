import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shop_app/providers/orders.dart';

class OrderItemWidget extends StatefulWidget {
  final OrderItem order;

  OrderItemWidget(this.order);

  @override
  _OrderItemWidgetState createState() => _OrderItemWidgetState();
}

class _OrderItemWidgetState extends State<OrderItemWidget> {
  var expandMore = false;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('${widget.order.amount}'),
            subtitle: Text('${widget.order.dateTime.toString()}'),
            trailing: IconButton(
              icon: Icon(expandMore ? Icons.expand_more : Icons.expand_less),
              onPressed: (){
                setState(() {
                  expandMore=!expandMore;
                });
              },
            ),
          ),
          if(expandMore)
            (

          Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
          height: min(widget.order.products.length * 20.0 + 10, 100),
          child:ListView(
              children: <Widget>[
                ...widget.order.products.map((prod)=>Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      prod.title,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      '${prod.quantity}x \$${prod.price}',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                      ),
                    )
                  ],
                ),).toList()
              ],
            )
    ,)
            )
        ],
      ),
    );
  }
}
