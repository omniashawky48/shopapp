import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/auth_provider.dart';
import 'package:shop_app/providers/product.dart';
import 'package:shop_app/providers/shopCart.dart';
import 'package:shop_app/screens/product_detail_screen.dart';

class ProductItem extends StatelessWidget {
  final isVendor;
  ProductItem(this.isVendor);
  @override
  Widget build(BuildContext context) {
    var product = Provider.of<Product>(context, listen: false);
    var Cart = Provider.of<CartProvider>(context, listen: false);
    var authData = Provider.of<Auth>(context, listen: false);
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
        child: InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(
                  ProductDetailsScreen.productDetailsScreenRoute,
                  arguments: product.id);
            },
            child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 3.0,
                          blurRadius: 5.0)
                    ],
                    color: Colors.white),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (!isVendor)
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Consumer<Product>(
                                builder: (con, product, _) => IconButton(
                                    icon: Icon(
                                      product.isfavourite
                                          ? Icons.favorite
                                          : Icons.favorite_border,
                                      color: Theme.of(context).accentColor,
                                    ),
                                    onPressed: () {
                                      product.toggleFavourite(
                                          authData.token, authData.userId);
                                    }),
                              ),
                            ]),
                      Hero(
                          tag: product.id,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                height: 100.0,
                                width: 100.0,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(product.imageUrl),
                                        fit: BoxFit.contain))),
                          )),
//                  SizedBox(height: 7.0),
                      Text(' \$ ${product.price.toString()}',
                          style: TextStyle(
                              color: Color(0xFFCC8053), fontSize: 14.0)),
                      Text(product.title,
                          style: TextStyle(
                              color: Color(0xFF575E67), fontSize: 16.0)),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Container(color: Color(0xFFEBEBEB), height: 1.0),
                      ),
                      Padding(
                          padding: EdgeInsets.all(5),
                          child: InkWell(
                            onTap: () {
                              Cart.addItem(
                                  product.id, product.title, product.price);
                              prefix0.Scaffold.of(context)
                                  .hideCurrentSnackBar();
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(translate('add_item_to_cart')),
                                duration: Duration(seconds: 3),
                                backgroundColor: Colors.amber,
                                action: SnackBarAction(
                                    label: translate('undo'),
                                    onPressed: () {
                                      Cart.removeSingleItem(product.id);
                                    }),
                              ));
                            },
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Icon(Icons.shopping_cart,
                                      size: 20, color: Color(0xFFD17E50)),
                                  Text(translate('add_to_cart'),
                                      style: TextStyle(
                                          color: Color(0xFFD17E50),
                                          fontSize: 16.0))
                                ]),
                          ))
                    ]))));
  }
}

//
//  Widget _item(){
//    return ClipRRect(
//      borderRadius: BorderRadius.circular(14),
//      child: GridTile(
//        child: GestureDetector(
//          onTap: (){
//            Navigator.of(context).pushNamed(ProductDetailsScreen.productDetailsScreenRoute,arguments: product.id);
//          },
//          child: Hero(
//            tag: product.id,
//            transitionOnUserGestures: true,
//            child: Image.network(
//              product.imageUrl,
//              fit: BoxFit.cover,
//            ),
//          ),
//        ),
//        footer: GridTileBar(
//          backgroundColor: Colors.black87,
//          leading: Consumer<Product>(
//            builder: (con,product,_)=>IconButton(
//                icon: Icon(
//                  product.isfavourite?
//                  Icons.favorite:
//                  Icons.favorite_border,
//                  color: Theme.of(context).accentColor,
//                ),
//                onPressed: () {
//                  product.toggleFavourite(authData.token,authData.userId);
//                }),
//          ),
//          trailing: IconButton(
//              icon: Icon(
//                Icons.shopping_cart,
//                color:Theme.of(context).accentColor,
//              ),
//              onPressed: () {
//                Cart.addItem(product.id, product.title, product.price);
//                prefix0.Scaffold.of(context).hideCurrentSnackBar();
//                Scaffold.of(context).showSnackBar(SnackBar(content:Text('Added Item To Card') ,duration:  Duration(seconds: 3),
//                  backgroundColor: Colors.amber,
//                  action:  SnackBarAction(label: "Undo", onPressed: () {  Cart.removeSingleItem( product.id);}),
//
//                ));
//              }),
//          title: Text(
//            product.title,
//            textAlign: TextAlign.center,
//          ),
//        ),
//      ),
//    );
//  }
