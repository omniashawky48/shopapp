import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/auth_provider.dart';
import 'package:shop_app/screens/orders_screen.dart';
import 'package:shop_app/screens/products_overview_screen.dart';
import 'package:shop_app/screens/user_products_screen.dart';

class AppDrawer extends StatelessWidget  {

final isvendor ;
AppDrawer({@required this.isvendor});

  @override
  Widget build(BuildContext context) {
    print('${isvendor} iiiii');
    return  Drawer(
        elevation: 2,
        child: Column(
          textDirection: TextDirection.ltr,
          children: <Widget>[
            SizedBox(
              height: 90,
              child: Container(
                color: Theme.of(context).primaryColor,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListTile(
                leading: Icon(Icons.shop),
                title: Text(translate('drawer.shop')),
                onTap: (){
                  Navigator.of(context).pushReplacementNamed(ProductsOverviewScreen.productsOverviewScreenRoute);
                },
              ),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListTile(
                leading: Icon(Icons.payment),
                title: Text(translate('drawer.order')),
                onTap: (){
                  Navigator.of(context).pushReplacementNamed(OrdersScreen.routeName);
                },
              ),
            ),
            Divider(),
            if(isvendor)
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListTile(
                leading: Icon(Icons.edit),
                title: Text(translate('drawer.manage_products')),
                onTap: (){
                  Navigator.of(context).pushReplacementNamed(UserProductsScreen.userRoute);
                },
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text(translate('drawer.logout')),
                onTap: (){
                  Navigator.of(context).pop();//to close drawer widget else will make error
                  Provider.of<Auth>(context,listen: false).logout();
                },
              ),
            ),
          ],
        )
    );
  }
}
